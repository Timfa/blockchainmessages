var displayRefreshing = false;
var didOnce = false;

setInterval(function()
{
    if(!displayRefreshing)
    {
        displayRefreshing = true;
        var xhttp = new XMLHttpRequest();
        var bc = this;
        xhttp.onreadystatechange=function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                var newJson = JSON.parse(this.responseText);

                displayRefreshing = false;
                
                for(var i = 0; i < newJson.length; i++)
                {
                    try
                    {
                        var datObj = JSON.parse(atob(newJson[i].data));

                        var element = document.getElementById("msg" + i);

                        element.getElementsByClassName("name")[0].innerText = datObj.name;
                        element.getElementsByClassName("txt")[0].innerText = datObj.text;

                        element.getElementsByClassName("hash")[0].innerText = "- " + newJson[i].hash;
                        element.getElementsByClassName("date")[0].innerText = " (" + newJson[i].timestamp + ")";
                    }
                    catch(e)
                    {

                    }
                }

                if(!didOnce)
                {
                    didOnce = true;
                    window.scrollTo(0,document.body.scrollHeight);
                }
            }
        };

        xhttp.open("GET", "getLastX.php?amount=21", true);
        xhttp.send();
    }
}, 1000)