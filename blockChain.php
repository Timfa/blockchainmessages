<?php
 header("Access-Control-Allow-Origin: *");

class Block
{
    function __construct($timestamp, $data, $previousHash)
    {
        $this->timestamp = $timestamp;
        $this->data = $data;
        $this->previousHash = $previousHash;

        $this->nonce = 0;

        $this->hash = $this->calculateHash();
    }

    static function FromJson($json)
    {
        //https://stackoverflow.com/a/20654216
        $stdobj = json_decode($json);  //JSON to stdClass
        $temp = serialize($stdobj);   //stdClass to serialized

        // Now we reach in and change the class of the serialized object
        $temp = preg_replace('@^O:8:"stdClass":@','O:5:"Block":', $temp);

        // Unserialize and walk away like nothing happend
        return unserialize($temp);   // Presto a php Class 
    }

    function calculateHash()
    {
        $hash = 0;
        $str = $this->previousHash . $this->timestamp . $this->data . $this->nonce;
        
        $hash = hash("SHA256", $str);

        $result = $hash;//strtoupper(base_convert($hash, 10, 32));
        echo $result;
        return $result;
    }
}

class BlockChain
{
    function __construct()
    {
        $this->fileName = "chain.json";
        $jsonStr = file_get_contents($this->fileName);

        $this->chain = json_decode($jsonStr);

        if($this->chain == null || count($this->chain) == 0)
        {
            $this->chain = [];
            array_push($this->chain, new Block("30-1-2011", "Genesis Block", ""));
            $this->save();
        }

        for($i = 0; $i < count($this->chain); $i++)
        {
            //https://stackoverflow.com/a/20654216
            $stdobj = $this->chain[$i];  //JSON to stdClass
            $temp = serialize($stdobj);  //stdClass to serialized

            // Now we reach in and change the class of the serialized object
            $temp = preg_replace('@^O:8:"stdClass":@','O:5:"Block":', $temp);

            // Unserialize and walk away like nothing happend
            $this->chain[$i] = unserialize($temp);   // Presto a php Class 
        }

        $this->difficultyKey = "123";
    }

    function save()
    {
        file_put_contents($this->fileName, json_encode($this->chain));
    }

    function isChainValid()
    {
        for($i = 1; $i < count($this->chain); $i++)
        {
            if($this->chain[$i]->hash !== $this->chain[$i]->calculateHash())
                return false;

            if($this->chain[$i]->previousHash !== $this->chain[$i - 1]->hash)
                return false;
        }
        
        if($this->chain[0]->calculateHash() !== $this->createGenesisBlock()->calculateHash())
            return false;

        return true;
    }

    function getLatestBlock()
    {
        return $this->chain[count($this->chain) - 1];
    }

    function addBlock($newBlock)
    {
        $dat = json_decode(base64_decode($newBlock->data));
        $name = $dat->name;

        if(strlen($name) < 1)
        {
            return false;
        }
        
        $dat->name = strip_tags($dat->name);
        $dat->text = strip_tags($dat->text);

        $newBlock->data = base64_encode(json_encode($dat));

        if($newBlock->hash === $newBlock->calculateHash())
        {
            if($this->getLatestBlock()->hash === $newBlock->previousHash)
            {
                if(substr($newBlock->hash, 0, strlen($this->difficultyKey)) === $this->difficultyKey)
                {
                    array_push($this->chain, $newBlock);
                    $this->save();
                    
                    return true;
                }
            }
        }

        return false;
    }
}