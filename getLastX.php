<?php
header("Access-Control-Allow-Origin: *");

$json = file_get_contents("chain.json");

$obj = json_decode($json);

$amt = 10;

if(isset($_GET["amount"]))
{
    $amt = intval($_GET["amount"]);
}

if($amt > count($obj))
    $amt = count($obj);

echo json_encode(array_slice($obj, -$amt, $amt));