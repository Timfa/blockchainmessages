var tryingHash = "";
class BlockChain
{
    constructor()
    {
        this.chain = [this.createGenesisBlock()];
        this.difficultyKey = "123";

        this.fetching = false;
    }

    fetchLastChainBlock(callback)
    {
        this.fetching = true;
        var xhttp = new XMLHttpRequest();
        var bc = this;
        xhttp.onreadystatechange=function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                var newJson = JSON.parse(this.responseText);

                var newBlock = new Block(newJson.timestamp, newJson.data);
                newBlock.previousHash = newJson.previousHash;
                newBlock.nonce = newJson.nonce;
                newBlock.hash = newJson.hash;
                bc.chain[0] = newBlock;
                
                bc.fetching = false;

                callback && callback();
            }
        };

        xhttp.open("GET", "getLast.php", true);
        xhttp.send();
    }

    isChainValid()
    {
        for(var i = 1; i < this.chain.length; i++)
        {
            if(this.chain[i].hash !== this.chain[i].calculateHash())
                return false;

            if(this.chain[i].previousHash !== this.chain[i-1].hash)
                return false;
        }
        
        if(this.chain[0].calculateHash() !== this.createGenesisBlock().calculateHash())
            return false;

        return true;
    }

    getLatestBlock()
    {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock)
    {
        newBlock.previousHash = this.getLatestBlock().hash;

        newBlock.mineBlock(this.difficultyKey);

        if(this.getLatestBlock().hash === newBlock.previousHash)
            this.chain.push(newBlock);
    }

    createGenesisBlock()
    {
        return new Block("30-1-2011", "Genesis Block", 0);
    }

    static getDateStr()
    {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var hh = String(today.getHours()).padStart(2, '0');
        var mm = String(today.getMinutes()).padStart(2, '0');

        return dd + '-' + mm + '-' + yyyy + ", " + hh + ":" + mm;
    }
}

class Block
{
    constructor(timestamp, data, previousHash = '')
    {
        this.timestamp = timestamp;
        this.data = data;
        this.previousHash = previousHash;

        this.nonce = 0;

        this.hash = this.calculateHash();
    }

    mineBlock(difficulty)
    {
        while(this.hash.substring(0, difficulty.length) !== difficulty)
        {
            var latestHash = chat.getLatestBlock().hash;
            if(this.previousHash != latestHash)
            {
                this.previousHash = latestHash;
                this.nonce = -1;
            }

            this.nonce++;
            this.hash = this.calculateHash();
            tryingHash = this.hash;
        }

        tryingHash = "";
    }

    calculateHash()
    {
        var hash = "";
        var str = this.previousHash.toString() + this.timestamp.toString() + this.data.toString() + this.nonce.toString();
        
        hash = sha256(str);

        return hash.toString()
    }
}

function PostMessage(text, name)
{
    if(isBusy)
        return;

    isBusy = true;

    chat.fetchLastChainBlock(function()
    {
        chat.addBlock(new Block(BlockChain.getDateStr(), btoa(JSON.stringify({text:text, name:name}))));

        var last = chat.getLatestBlock();

        var lastJson = JSON.stringify(last);
        var base64 = btoa(lastJson);

        var xhttp = new XMLHttpRequest();
        var bc = this;
        xhttp.onreadystatechange=function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                document.getElementById("hashAttempt").innerText = "Done!";
            }
            
            isBusy = false;
            didOnce = false;
        };

        xhttp.open("GET", "post.php?message=" + base64, true);
        xhttp.send();
    });
}

function postmsg()
{
    setTimeout(function()
    { 
        var name = document.getElementById("inputname").value;

        if(name.trim() == "")
            name = "Anonymous";

        PostMessage(document.getElementById("input").value, name);

        document.getElementById("input").value = "";
    }, 100);
    
    document.getElementById("hashAttempt").innerText = "Mining a new block for your message!"
}

setInterval(function()
{
    if(!chat.fetching)
    {
        chat.fetchLastChainBlock();
    }
}, 500)

var chat = new BlockChain();
var isBusy = false;