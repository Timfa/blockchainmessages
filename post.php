<?php
header("Access-Control-Allow-Origin: *");

include 'blockChain.php';

$blockChain = new BlockChain();
$result = false;
$message = "Invalid input: No input detected.";

if(isset($_GET["message"]))
{
    $msg = $_GET["message"];

    try 
    {
        $json = base64_decode($msg);
        $newBlock = Block::FromJson($json);

        $result = $blockChain->addBlock($newBlock);

        if(!$result)
        {
            $message = "Invalid input: Input block rejected.";
        }
        else
        {
            $message = "Block Accepted.";
        }
    } 
    catch (Exception $e) 
    {
        $message = "Invalid input: Bad Block.";
    }
}

if($result)
{
    http_response_code(200);
}
else
{
    http_response_code(400);
}

echo $message;